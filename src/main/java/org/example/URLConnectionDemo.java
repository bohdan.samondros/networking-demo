package org.example;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionDemo {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://www.google.com");
            URLConnection urlcon = url.openConnection();
            InputStream stream = urlcon.getInputStream();
            int i;
            while ((i = stream.read()) != -1) {
                System.out.print((char) i);
            }
            System.out.println();
            System.out.println();
            for (int k = 1; k <= 8; k++) {
                System.out.println(urlcon.getHeaderFieldKey(k) + " = " + urlcon.getHeaderField(k));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
