package org.example;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class HttpConnectionDemo {
    private static CookieManager cookieManager = new CookieManager();

    public static void main(String[] args) throws IOException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("q", "Java");
        URL url = new URL("http://www.google.com/search?" + ParameterStringBuilder.getParamsString(parameters));
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        // Request method
        con.setRequestMethod("GET");

        // Headers
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Content-Type", "text/plain");

        // Timeouts
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);

        int status = con.getResponseCode();

        System.out.println("Http status: " + status);

        // get cookies
        String cookieHeader = con.getHeaderField("Set-Cookie");
        List<HttpCookie> httpCookies = HttpCookie.parse(cookieHeader);
        httpCookies.forEach(cookie -> {
            System.out.println("Cookie: " + cookie);
            try {
                cookieManager.getCookieStore().add(con.getURL().toURI(), cookie);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

/*        con.setRequestProperty("Cookie",
                String.join(";", cookieManager.getCookieStore()
                        .getCookies()
                        .stream()
                        .map(Objects::toString)
                        .toArray(String[]::new)));*/


        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        System.out.println("Body:\n" + content);
        in.close();

    }

    public static class ParameterStringBuilder {
        public static String getParamsString(Map<String, String> params)
                throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();

            for (Map.Entry<String, String> entry : params.entrySet()) {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                result.append("&");
            }

            String resultString = result.toString();
            return resultString.length() > 0
                    ? resultString.substring(0, resultString.length() - 1)
                    : resultString;
        }
    }
}
