package org.example;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class SocketClientDemo {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 6666);
        try (socket;
             DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
             Scanner scanner = new Scanner(System.in)) {
            String input;
            while (!(input = scanner.nextLine()).isEmpty()) {
                dout.writeUTF(input);
            }
            dout.writeUTF("");
            dout.flush();
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
}
